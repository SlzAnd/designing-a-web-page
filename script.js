const addToCartButtons = document.querySelectorAll('.btn');
const cartCount = document.querySelector('.cart-count');
let itemCount = 0;

addToCartButtons.forEach((button) => {
  button.addEventListener('click', (event) => {
    event.preventDefault();
    itemCount++;
    cartCount.innerText = itemCount;
  });
});